var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Hello from posts (list all)');
});

router.get('/:id', function(req, res, next) {
	res.send('Hello from posts' + req.params.id);
});

module.exports = router;
